Here's how to use it.
Copy/Download the file to a directory you want to keep it in.
Copy/Download the course xml you want to test to a working directory (this does not include books.xml)
On the command line run the ruby program with the following command:
ruby convertDCXMLtoLinks.rb --bfp=full/path/to/where/you/copied/the/xml/
If you only want to test a specific xml file run:
ruby convertDCXMLtoLinks.rb --bfp=full/path/to/where/you/copied/the/xml/ --file=theFileName.xml
This will create a directory ("Links") in the base directory used in the bfp parameter and create two html files within it. The first file is a list of all links checked. The second file is a list of all links that could not be verified (this does not necessarily mean 404).

Example:
ruby convertDCXMLtoLinks.rb --bfp=C:\Users\jklins\Documents\sandbox\DynamicClassroomXML2016 --file=g7a_menu_teacher.xml