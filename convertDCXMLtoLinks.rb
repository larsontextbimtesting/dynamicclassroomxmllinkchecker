begin
  require 'rubygems'
rescue LoadError
  puts "Required gem rubygems not found. Installing now..."
  `gem install rubygems`
  puts "done installing rubygems"
  require 'rubygems'
end
begin
  require 'xmlsimple'
rescue LoadError
  puts "Required gem xmlsimple not found. Installing now..."
  `gem install xmlsimple`
  puts "done installing xmlsimple"
  require 'xmlsimple'
end
begin
  require 'net/https' #use https or http depending on use- also see code below
rescue LoadError
  puts "Required gem net/https not found. Installing now..."
  `gem install net/https`
  puts "done installing net/https"
  require 'net/https'
end

$args = {}

def PrintHelpMessage()
	print "\nplease run the script with the following options:\n"
	print "BASE FILE PATH: (REQ):    '--bfp=base/File/Path/' this is the location of\n"
	print "                                                  the cartridge spreadsheets\n\n"
	print "FILENAME (OPT):           '--file=somefile.xml' if you wish to only test a \n"
	print "                                                 specific file\n\n"
	abort
end

begin
	ARGV.each do |arg|
	match = /--(?<key>.*?)=(?<value>.*)/.match(arg)
	$args[match[:key]] = match[:value] # e.g. args['first_name'] = 'donald'
	end
	if ($args['bfp'].nil?)
		PrintHelpMessage()
	end
rescue
	PrintHelpMessage()
end
$baseDir = File.join($args['bfp'], "")
if($baseDir.include?("\\"))
	$baseDir = $baseDir.gsub!("\\", "/")
end

if(!File.exists?($baseDir))
	print "invalid base file path\n"
	abort
end

$testFiles = $args['file'].nil? ? '*.xml*' : $args['file']

def CheckLink(linkInfo, origString, urlString, limit, line, fileout, fileoutNotFound, getRequest = false)
	begin	
		url = URI.parse(urlString)
		http = Net::HTTP.new(url.host, url.port)
		if (urlString.index("https") == 0)	
				http.use_ssl = true
				http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		if (getRequest == false) #This if/else is the best I could do to handle redirects and 404s :( ...There is a better way, I'm sure of it!
			request = Net::HTTP::Head.new(url.request_uri)
			request['Cookie'] = "loggedin=1,teacher=1"
			r = http.request(request)
		else
			r = http.get(url.request_uri)
			cookie = {'Cookie'=>r.to_hash['set-cookie'].collect{|ea|ea[/^.*?;/]}.join}
			r = http.get(url.request_uri,cookie)
		end
	rescue
		p "something went wrong with this request: #{$!.backtrace}  #{line}: #{urlString}"
		fileoutNotFound.puts "<div class='all somethingWentWrong'>something went wrong with this request (rescued): | Line: #{line} |#{linkInfo}|<a href ='#{origString}'>#{origString}</a>| #{urlString} | #{$!.backtrace} <br/></div>"
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		return
	end
	case r
		when Net::HTTPSuccess     then 
			print "#{line}, "
		when Net::HTTPRedirection then 
			#p "redirected  Line: #{line} #{origString}- > #{r['location']}"
			if((r['location'] == '/BIM/login') and (limit > 0))
				CheckLink(linkInfo, origString, origString, limit - 1, line, fileout, fileoutNotFound, true)
			else
				CheckLink(linkInfo, origString, r['location'], limit - 1, line, fileout, fileoutNotFound, true)
			end
		when Net::HTTPNotFound
			$brokenLinks = $brokenLinks + 1
			$allbrokenLinks = $allbrokenLinks + 1
			fileoutNotFound.puts "<div class= 'all notFound' >failed checking -NOT FOUND - (#{r.code}) | Line: #{line} |#{linkInfo}|<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/></div>"
			p "failed checking -NOT FOUND - (#{r.code})- Line: #{line} | #{origString}"
		else
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		fileoutNotFound.puts "<div class='all somethingWentWrong'>failed checking (#{r.code})- | Line: #{line} |#{linkInfo}|<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><<br/></div>"
	end
end



checkedLinks = 0
$brokenLinks = 0
$jquery = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>"
$buttons = "<div><button id='AllLines'>All</button><button id='NotFound'>Not Found</button><button id='IncorrectProtocol'>Incorrect Protocol (http)</button><button id='SomethingWentWrong'>Something went wrong</button><button id='ErrorDetails'>Show or Hide Error Details</button><div><br/>"
$js = "<script>$('#AllLines').click(function (){$('.all').show();}); $('#NotFound').click(function (){$('.all').hide();$('.notFound').show();}); $('#IncorrectProtocol').click(function (){$('.all').hide();$('.incorrectProtocol').show()});$('#SomethingWentWrong').click(function (){$('.all').hide();$('.somethingWentWrong').show()});$('#ErrorDetails').click(function (){$('.errorDetails').toggle();});</script>"

time=Time.now.to_i
directory_name = "#{$baseDir}Links"
Dir.mkdir(directory_name) unless File.exists?(directory_name)
fileoutName = "#{$baseDir}Links/AllLinksDynamicClassroom#{time}.html"
fileout = File.open(fileoutName, "w")
fileout.puts "<!DOCTYPE html><html><head><title>Dynamic Classroom Links</title></head><body>"

fileoutNameNotFound = "#{$baseDir}Links/LinksNotFoundDynamicClassroom#{time}.html"
fileoutNotFound = File.open(fileoutNameNotFound, "w")
fileoutNotFound.puts "<!DOCTYPE html><html><head><title>Dynamic Classroom  2016 NotFound - All Links</title>#{$jquery}</head><body>#{$buttons}"

Dir.glob("#{$baseDir}#{$testFiles}") do |item|
  p "Testing Links for #{item}"
  fileout.puts "<h1>#{item}</h1>"
  fileoutNotFound.puts "<h1>#{item}</h1>"
  lastErrorItem = ''
  lastErrorChapAndSection = ''
  #p "<h2>#{item}</h2>"
  myXml = XmlSimple.xml_in(item)
 # p myXml
  myXml["chapter"].each do |chapter|
		chapter["section"].each do |section|
			section["resource"].each do |resource|
				checkedLinks = checkedLinks + 1
				if (resource["href"].index("http") == 0)
					link = "#{resource["href"]}"
					linkInfo = "#{myXml["name"]} | #{section["label"]} | #{resource["name"]}"
					fileoutNotFound.puts "<div class='all incorrectProtocol'>http: | Link#: #{checkedLinks} |#{linkInfo}|<a href ='#{link}'>#{link}</a>|<br/></div>"
				else
					link = "https:#{resource["href"]}"
				end 
				linkInfo = "#{myXml["name"]} | #{section["label"]} | #{resource["name"]}"		
				begin
					fileout.puts "Link#: #{checkedLinks}: | <a href ='#{link}'>#{link}</a><br/>"
					CheckLink(linkInfo, link, link, 10, checkedLinks, fileout, fileoutNotFound)
				rescue
					$brokenLinks = $brokenLinks + 1
					p "failed checking (rescued)- Link#: #{checkedLinks}: | <a href ='#{link}'>#{link}</a><br/>"
					#abort
					fileoutNotFound.puts "<div class='all somethingWentWrong'>failed checking (rescued):<div class='errorDetails'> #{$!.backtrace} </div> | Link#: #{checkedLinks} |#{linkInfo}|<a href ='#{link}'>#{link}</a>|<br/></div>"
				end	
			end	
		end 
	end
  end
  
  
=begin
	#p chap["href"]
		url = URI.parse(chap["href"])
        req = Net::HTTP.new(url.host, url.port)
				res = req.request_head(url.path)
				res = Net::HTTP.start(
				:use_ssl => uri.scheme == 'https', 
				:verify_mode => OpenSSL::SSL::VERIFY_NONE) do |https|
		https.request(req)
		end
=end		

 fileout.puts "<h2>Total Checked Links: #{checkedLinks}</h2>"
 fileout.puts "<h2>Total Broken Links: #{$brokenLinks}</h2>"
 fileout.puts "</body></html>"
 p "Total Checked Links: #{checkedLinks}"
 p "Total Broken Links: #{$brokenLinks}"
 fileout.close
 fileoutNotFound.puts "<h2 style='color:blue'>Total Checked Links: #{checkedLinks}</h2>"
 fileoutNotFound.puts "<h2 style='color:red'>Total Broken Links: #{$brokenLinks}</h2>"
 fileoutNotFound.puts "#{$js}</body></html>"
 fileoutNotFound.close
 
 
